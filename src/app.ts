import { IBoot } from 'egg'

export default class AppBoot implements IBoot {
  async serverDidReady() {
    console.log('■■■■■■■■■■■■■■■■■■■ 就绪 ■■■■■■■■■■■■■■■■■■')
    process.title = 'midway-api'
  }
}
