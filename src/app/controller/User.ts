import { controller, get, provide, Context, inject } from 'midway'
import { UserService } from '../service/UserService'

@provide()
@controller('/api/user')
export class UserController {
  @inject()
  ctx: Context

  @inject()
  userService: UserService

  @get('/info')
  async api() {
    console.log(this.ctx.path)
    if ('/api/user/info' != this.ctx.path) {
      console.log('ctx 串请求了')
    }
    this.ctx.body = await this.userService.userinfo()
  }
}
