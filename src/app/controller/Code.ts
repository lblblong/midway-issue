import { controller, get, provide, Context, inject } from 'midway'
import { CodeService } from '../service/CodeService'

@provide()
@controller('/api/code')
export class CodeController {
  @inject()
  ctx: Context

  @inject()
  codeService: CodeService

  @get('/list')
  async api() {
    console.log(this.ctx.path)
    if ('/api/code/list' != this.ctx.path) {
      console.log('ctx 串请求了')
    }
    this.ctx.body = await this.codeService.list()
  }
}
