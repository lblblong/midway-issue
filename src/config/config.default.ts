export = (appInfo: any) => {
  const config: any = {}

  config.cluster = {
    listen: {
      port: 8080,
      hostname: '0.0.0.0'
    }
  }
  
  // 中间件配置
  config.middleware = ['sleep']

  // app key
  config.keys = appInfo.name

  config.cors = {
    origin: '*',
    allowMethods: 'GET,HEAD,PUT,POST,DELETE,PATCH'
  }

  return config
}
