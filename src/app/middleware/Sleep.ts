import { Context } from 'midway'
import sleep from '../../packages/Sleep'

export default () => {
  return async function transaction(ctx: Context, next) {
    try {
      await sleep(1000)
      await next()
    } catch (err) {
      throw err
    }
  }
}
